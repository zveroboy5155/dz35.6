#include <iostream>
#include "DZ31.5.h"

int main() {
	int cmd = -1;

	while (cmd != 0) {
		std::cout << "Enter task number( 1 - 3) or 0 for exit" << std::endl;
		std::cin >> cmd;
		if (cmd == 1) task1();
		else if (cmd == 2) task2();
		else if (cmd == 3) task3();
		}
    
    return 0;
}
