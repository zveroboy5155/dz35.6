#include "DZ31.5.h"
//#include <thread>
#include <chrono>
#include <ctime>
#include <unordered_set>

#define DBG(code) code

void task2() {
	std::vector<int> vec{1, 2, 3, 4, 3, 1, 2, 2};
	auto unic_vec = [](std::vector<int> &vec) {
		std::vector<int> unicVec;
		std::shared_ptr<std::vector<int>> res;
		std::unordered_set<int> unic;

		for (auto &it : vec) {
			if (unic.count(it)) continue;
			unic.insert(it);
			unicVec.push_back((int)it);
		}
		res = std::make_shared<std::vector<int>>(unicVec);
		return res;
	};

	auto ptr = unic_vec(vec);

	for (auto it : *ptr) {
		std::cout << it << std::endl;
	}
}