#include "DZ31.5.h"
#include <filesystem>

auto recursiveGetFileNamesByExtension =
[](std::filesystem::path path,
	const std::string extension) {
	std::vector<std::filesystem::path> names;
	std::string cur_name;

	for (auto& p : std::filesystem::recursive_directory_iterator(path)){
		if (p.is_directory()) continue;
		if (p.path().extension().compare(extension)) continue;
		names.push_back(p.path().filename());
	}
	return names;
};

void task3() {
	std::filesystem::path path{ "G:\\skillbox" };
	
	setlocale(0, " ");
	auto files = recursiveGetFileNamesByExtension(path, ".h");

	for (auto file : files)
		std::cout << file << std::endl;

	return;
}