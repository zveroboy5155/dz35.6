#include "DZ31.5.h"
#include <map>
#include <array>

void task1() {
	std::array<int, 5> arr = {1,2,3,4,5};

	for (auto it : arr) std::cout << it << std::endl;
}