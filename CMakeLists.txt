cmake_minimum_required(VERSION 3.12)
project(DZ35_6)

set(CMAKE_CXX_STANDARD 17)

include_directories(./)


# Добавьте источник в исполняемый файл этого проекта.
file(GLOB SOURCES src/*.cpp) 

add_executable(DZ_33.5 ${SOURCES})
